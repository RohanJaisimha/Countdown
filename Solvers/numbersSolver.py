import itertools
import sys
import random
import copy


def numSolverHelper(exprs, target):
    for expr in exprs:
        # print(expr, eval(expr))
        if eval(expr) == target:
            expr = expr.replace("//", "/")
            expr = expr.replace("*", chr(215))
            return expr + " = " + str(target)
    if len(exprs) != 1:
        for expr1, expr2 in itertools.permutations(exprs, 2):
            new_exprs = copy.deepcopy(exprs)
            new_exprs.remove(expr1)
            new_exprs.remove(expr2)
            expr1_eval = eval(expr1)
            expr2_eval = eval(expr2)
            repetitions = {expr1_eval, expr2_eval}
            if (expr1_eval * expr2_eval) not in repetitions:
                ans = numSolverHelper(
                    new_exprs + ["(" + expr1 + " * " + expr2 + ")"], target
                )
                if len(ans) != 0:
                    return ans
            if (expr1_eval + expr2_eval) not in repetitions:
                ans = numSolverHelper(
                    new_exprs + ["(" + expr1 + " + " + expr2 + ")"], target
                )
                if len(ans) != 0:
                    return ans
            if expr1_eval > expr2_eval and (expr1_eval - expr2_eval) not in repetitions:
                ans = numSolverHelper(
                    new_exprs + ["(" + expr1 + " - " + expr2 + ")"], target
                )
                if len(ans) != 0:
                    return ans
            if (
                expr1_eval % expr2_eval == 0
                and (expr1_eval // expr2_eval) not in repetitions
            ):
                ans = numSolverHelper(
                    new_exprs + ["(" + expr1 + " // " + expr2 + ")"], target
                )
                if len(ans) != 0:
                    return ans
    return ""


def numSolver(nums, target):
    exprs = list(map(str, nums))
    return numSolverHelper(exprs, target)


def main():
    p = random.uniform(0, 1)
    num_big = 0  # 2 if p < 0.7 else random.randrange(1, 5)
    num_small = 6 - num_big
    big_nums = [i * 25 for i in range(1, 5)]
    small_nums = [i for i in range(1, 11) for j in range(2)]
    nums = random.sample(big_nums, num_big) + random.sample(small_nums, num_small)
    nums.sort(reverse=True)
    target = random.randrange(100, 1000)
    print(" ".join(map(str, nums)), "\t", target)
    print(numSolver(nums, target))


if __name__ == "__main__":
    main()
