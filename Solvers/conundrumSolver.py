from itertools import permutations
from utility import getWords


def conundrumSolver(letters):
    words = getWords()
    answers = set()

    for perm in permutations(letters):
        perm = "".join(perm)
        if perm in words:
            answers.add(perm)

    return answers


def main():
    print("\n".join(conundrumSolver(list("abdicates"))))


if __name__ == "__main__":
    main()
