def getWords():
    fin = open("countdownDictionary.txt", "r")

    words = {line.strip() for line in fin}

    fin.close()

    return words


def main():
    print(getWords())


if __name__ == "__main__":
    main()
