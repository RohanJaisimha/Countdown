import random


def giveChoice():
    return input("Big or Small? (B / S)").lower()


def main():
    big_nums = [25 * i for i in range(1, 5)]
    small_nums = [i for i in range(1, 11) for j in range(2)]

    for i in range(6):
        choice = giveChoice()


if __name__ == "__main__":
    main()
