def makeCountdownDictionary():
    fin = open("words.txt", "r")
    fout = open("countdownDictionary.txt", "w")
    for line in fin:
        word = line.strip()
        if len(word) <= 9:
            fout.write(word + "\n")
    fout.close()
    fin.close()


def main():
    makeCountdownDictionary()


if __name__ == "__main__":
    main()
